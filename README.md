## Project created with create react app

### How to create credentials
* Go to [https://openweathermap.org/api](https://openweathermap.org/api) and Click **Sign In** to create an account.

* Once you have an account get an API key from the **API Key** menu on your dashboard.

* Return to this project and in the **src** folder, create a file *credentials.js*

* Add your API key in that file as below:
```javascript
export const appid = <YOUR API KEY>;
```

* You should now be able to run this web app, enjoy.