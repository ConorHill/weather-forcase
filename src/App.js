import React from 'react'
import FiveDay from './containers/FiveDay/FiveDay'

export default function App() {
  return (
    <>
      <FiveDay />
    </>
  )
}
