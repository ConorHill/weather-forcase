import React from "react";
import cloudySun from "../../assets/images/cloudy-sun.png";
import heavyRain from "../../assets/images/heavy-rain.png";
import lightRain from "../../assets/images/light-rain.png";
import overcast from "../../assets/images/overcast.png";
import sunny from "../../assets/images/sunny.png";

import "./WeatherCard.css";

const weatherCard = ({ active, day, low, high, onClick, weather }) => {
  let img;
  let containerStyle = ["weather__container", active ? "active" : ""].join(" ");  
  switch (weather) {
    case "broken clouds":
      img = <img className="weather-icon" src={cloudySun} alt="Weather Icon" />;
      break;
    case "scattered clouds":
      img = <img className="weather-icon" src={cloudySun} alt="Weather Icon" />;
      break;
    case "overcast clouds":
      img = <img className="weather-icon" src={overcast} alt="Weather Icon" />;
      break;

    case "light rain":
      img = <img className="weather-icon" src={lightRain} alt="Weather Icon" />;
      break;

    case "heavy rain":
      img = <img className="weather-icon" src={heavyRain} alt="Weather Icon" />;
      break;

    default:
      img = <img className="weather-icon" src={sunny} alt="Weather Icon" />;
      break;
  }

  return (
    <div className={containerStyle} onClick={onClick}>
      <h3 className="weather__title">{day}</h3>
      <div className="weather-icon__container">
        {img}
      </div>
      <div className="row">
        <p className="weather__temp--high">{high}</p>
        <p className="weather__temp--low">{low}</p>
      </div>
    </div>
  );
};

export default weatherCard;
