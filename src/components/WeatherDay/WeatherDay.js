import React from 'react'

export const WeatherDay = ({day, time, children, weather}) => {
  return (
    <div className="day__container">
      <h3 className="day__title">{day}</h3>
      <h4 className="day__subtitle">{time}</h4>
      {children}
      <p style={{textAlign: "center"}}>{weather}</p>
    </div>
  )
}
