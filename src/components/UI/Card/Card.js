import React from "react";

import "./Card.css";

export const Card = ({ children }) => {
  return (
    <div className="card__container">
      <div className="card">{children}</div>
    </div>
  );
};
