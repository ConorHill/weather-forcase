import React from "react";
import {Card, WeatherDay, WeatherCard} from '../../components'

const Details = ({ogDay}) => {
  return (
    <Card key={ogDay.id}>
      <WeatherDay
        day={ogDay.day}
        time={ogDay.time}
        weather={ogDay.weather.description}
      >
        <WeatherCard
          low={ogDay.low}
          high={ogDay.high}
          weather={ogDay.weather.description}
        />
      </WeatherDay>
    </Card>
  );
};

export default Details
