import React, { Component } from "react";
import { connect } from "react-redux";
import { getData, onClick } from "../../store/weatherActions";

import {WeatherCard} from "../../components";
import Details from '../Details/Details'

import "./FiveDay.css";

class FiveDay extends Component {
  componentDidMount() {
    this.props.onStart();
  }

  render() {
    let days = <p>Gathering Weather Data</p>;
    let details = "";
    let detailStyle = "five-days__details--none";

    if (this.props.loading === false) {
      days = this.props.week.map((day, index) => {
        if (day.active) {
          details = this.props.ogData.map(ogDay => {
            if (ogDay.day === day.day)
              return (
                <Details ogDay={ogDay} />
              );
            return null;
          });
          detailStyle = "five-days__details";
        }
        return (
          <WeatherCard
            key={day.id}
            active={day.active}
            day={day.day}
            low={day.low}
            high={day.high}
            onClick={() => this.props.onClick(index)}
            weather={day.weather.description}
          />
        );
      });
    }

    return (
      <div className="five-days__container">
        {days}
        <div className={detailStyle}>{details}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ogData: state.ogData,
  week: state.week,
  loading: state.loading
});

const mapDispatchToProps = dispatch => ({
  onStart: () => dispatch(getData()),
  onClick: index => dispatch(onClick(index))
});

export default connect(mapStateToProps, mapDispatchToProps)(FiveDay);
