import * as actionTypes from "./actionTypes";
import { appid } from "../credentials";
import axios from "axios";

const startGet = () => ({
  type: actionTypes.START_SEVENDAY
});

const getSuccess = data => ({
  data: data,
  type: actionTypes.SEVENDAY_SUCCESS
});

const getFailure = e => ({
  error: e,
  type: actionTypes.SEVENDAY_FAILURE
});

export const onClick = index => ({
  type: actionTypes.ACTIVE,
  index: index
})

export const getData = () => {
  return dispatch => {
    dispatch(startGet());
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/forecast/?q=dublin,ie&cnt=&units=metric&appid=${appid}`
      )
      .then(res => dispatch(getSuccess(res.data.list)))
      .catch(e => dispatch(getFailure(e)));
  };
};
