import * as actionTypes from "./actionTypes";

const initialState = {
  ogData: [],
  week: [],
  error: "",
  loading: false
};

const createWeek = (state, action) => {
  const data = action.data
    .map(i => {
      return {
      id: i.dt,
      day: new Intl.DateTimeFormat("en-US", { weekday: "long" }).format(
        i.dt * 1000
      ),
      time: `${new Date(i.dt*1000).getHours()}:${new Date(i.dt*1000).getMinutes()}0`,
      high: i.main.temp_min,
      low: i.main.temp_max,
      weather: i.weather[0],
      active: false
    }})

    const week = data.filter(
      (day, index) => day.day !== (index === 0 ? "" : data[index - 1].day)
    );

  return { ...state, week: week, ogData: data, loading: false };
};

const updateWeek = (state, action) => {
  const updatedWeek = state.week.map((item, index) => {
    if (index !== action.index) return item;
    return {
      ...item,
      active: !item.active
    };
  });


  return { ...state, week: updatedWeek };
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.START_SEVENDAY:
      return { ...state, loading: true };
    case actionTypes.SEVENDAY_SUCCESS:
      return createWeek(state, action);
    case actionTypes.SEVENDAY_FAILURE:
      return { ...state, loading: false, error: action.error };
    case actionTypes.ACTIVE:
      return updateWeek(state, action);
    default:
      return state;
  }
};

export default reducer;
